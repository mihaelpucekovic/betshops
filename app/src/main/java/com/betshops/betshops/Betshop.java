package com.betshops.betshops;

import com.google.gson.annotations.SerializedName;

public class Betshop {

    @SerializedName("Features")
    private String[] Features;
    @SerializedName("Id")
    private Integer Id;
    @SerializedName("Name")
    private String Name;
    @SerializedName("Location")
    private LocationBetshop LocationBetshop;
    @SerializedName("Address")
    private String Address;
    @SerializedName("CityId")
    private Integer CityId;
    @SerializedName("City")
    private String City;
    @SerializedName("County")
    private String County;

    public Betshop(String[] Features, Integer Id, String Name, LocationBetshop LocationBetshop, String Address, Integer CityId, String City, String County) {
        this.Features = Features;
        this.Id = Id;
        this.Name = Name;
        this.LocationBetshop = LocationBetshop;
        this.Address = Address;
        this.CityId = CityId;
        this.City = City;
        this.County = County;
    }

    public String[] getFeatures() {
        return Features;
    }

    public void setFeatures(String[] Features) {
        this.Features = Features;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public Double getLocationLat() {
        return LocationBetshop.getLat();
    }

    public void setLocationLat(LocationBetshop LocationBetshop) {
        this.LocationBetshop.setLat(LocationBetshop.getLat());
    }

    public Double getLocationLng() {
        return LocationBetshop.getLng();
    }

    public void setLocationLng(LocationBetshop LocationBetshop) {
        this.LocationBetshop.setLng(LocationBetshop.getLng());
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public Integer getCityId() {
        return CityId;
    }

    public void setCityId(Integer CityId) {
        this.CityId = CityId;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public String getCounty() {
        return County;
    }

    public void setCounty(String County) {
        this.County = County;
    }
}
