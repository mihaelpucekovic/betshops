package com.betshops.betshops;

import com.google.gson.annotations.SerializedName;

public class LocationBetshop {
    @SerializedName("Lat")
    private Double Lat;
    @SerializedName("Lng")
    private Double Lng;

    public LocationBetshop(Double Lat, Double Lng) {
        this.Lat = Lat;
        this.Lng = Lng;
    }

    public Double getLat() {
        return Lat;
    }

    public void setLat(Double Lat) {
        this.Lat = Lat;
    }

    public Double getLng() {
        return Lng;
    }

    public void setLng(Double Lng) {
        this.Lng = Lng;
    }
}
