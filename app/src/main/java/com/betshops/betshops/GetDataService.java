package com.betshops.betshops;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetDataService {

    @GET("/api/OMh3VFFoNEq8c4Ql/1/de/util/shops?boundingBox=60.76589813839328,10.83353750942734,45.81088354701596,15.99481938598126")
    Call<List<Betshop>> getAllBetshops();
}
