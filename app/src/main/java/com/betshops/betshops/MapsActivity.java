package com.betshops.betshops;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowCloseListener, OnMapReadyCallback {

    private GoogleMap mMap;
    private TextView textViewAddress;
    private TextView textViewPhoneNumber;
    private TextView textViewHours;
    private List<Betshop> betshopList;
    private String routeAddress = "";
    private final int PERMISSION_REQUEST_LOCATION = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        textViewAddress = (TextView) findViewById(R.id.textViewAddress);
        textViewPhoneNumber = (TextView) findViewById(R.id.textViewPhoneNumber);
        textViewHours = (TextView) findViewById(R.id.textViewHours);
        ImageButton imageButtonLocation = (ImageButton) findViewById(R.id.imageButtonLocation);
        ImageButton imageButtonClose = (ImageButton) findViewById(R.id.imageButtonClose);
        ImageButton imageButtonPhone = (ImageButton) findViewById(R.id.imageButtonPhone);
        ImageButton imageButtonHours = (ImageButton) findViewById(R.id.imageButtonHours);
        Button buttonRoute = (Button) findViewById(R.id.buttonRoute);

        Drawable mDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bet_shop_location).mutate();
        mDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor("#8cbb15"), PorterDuff.Mode.SRC_ATOP));
        imageButtonLocation.setImageDrawable(mDrawable);

        mDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_close).mutate();
        mDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor("#bdbdbd"), PorterDuff.Mode.SRC_ATOP));
        imageButtonClose.setImageDrawable(mDrawable);

        mDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bet_shop_phone).mutate();
        mDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor("#8cbb15"), PorterDuff.Mode.SRC_ATOP));
        imageButtonPhone.setImageDrawable(mDrawable);

        mDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bet_shop_hours).mutate();
        mDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor("#8cbb15"), PorterDuff.Mode.SRC_ATOP));
        imageButtonHours.setImageDrawable(mDrawable);

        imageButtonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (findViewById(R.id.linearLayoutDetails).getVisibility() == View.VISIBLE) {
                    findViewById(R.id.linearLayoutDetails).setVisibility(View.GONE);
                }
            }
        });

        buttonRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("geo:0,0?q=" + routeAddress);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        getData();

        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowCloseListener(this);

        if (enabledLocationPermission()) {
            setLocation();
        }
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        Integer id = (Integer) marker.getTag();

        marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_active));

        for (Betshop betshop : betshopList) {
            if (id == betshop.getId()) {
                textViewAddress.setText(betshop.getAddress());

                Calendar calendar = Calendar.getInstance();

                if (calendar.get(Calendar.HOUR_OF_DAY) >= 8 && calendar.get(Calendar.HOUR_OF_DAY) < 16) {
                    textViewHours.setTextColor(Color.parseColor("#8cbb15"));
                    textViewHours.setText("Open now until 16:00");
                }
                else {
                    textViewHours.setTextColor(Color.parseColor("#505050"));
                    textViewHours.setText("Opens tomorrow at 8:00");
                }

                routeAddress = betshop.getAddress();

                break;
            }
        }

        if (findViewById(R.id.linearLayoutDetails).getVisibility() == View.GONE) {
            findViewById(R.id.linearLayoutDetails).setVisibility(View.VISIBLE);
        }

        return false;
    }

    @Override
    public void onInfoWindowClose(Marker marker) {
        marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_normal));

        if (findViewById(R.id.linearLayoutDetails).getVisibility() == View.VISIBLE) {
            findViewById(R.id.linearLayoutDetails).setVisibility(View.GONE);
        }
    }

    private void getData() {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Betshop>> call = service.getAllBetshops();
        call.enqueue(new Callback<List<Betshop>>() {
            @Override
            public void onResponse(Call<List<Betshop>> call, Response<List<Betshop>> response) {
                addMarkersToMap(response.body());
            }

            @Override
            public void onFailure(Call<List<Betshop>> call, Throwable throwable) {
                Toast.makeText(MapsActivity.this, "Something went wrong, please try later.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addMarkersToMap(List<Betshop> betshopList) {
        this.betshopList = betshopList;

        for (Betshop betshop : betshopList) {
            LatLng latLng = new LatLng(betshop.getLocationLat(), betshop.getLocationLng());
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(betshop.getName())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_normal)));
            marker.setTag(betshop.getId());
        }
    }

    public boolean enabledLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                new AlertDialog.Builder(this)
                        .setTitle("Permission to access location")
                        .setMessage("You need to give permission to access location to use all the functionality in the application.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(MapsActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        PERMISSION_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();
            }
            else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_REQUEST_LOCATION);
            }

            return false;
        }
        else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setLocation();
                } else {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(48.137154, 11.576124), 5.5f));
                }
            }

        }
    }

    private void setLocation() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();
            Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
         
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 5.5f));
        }
    }
}
